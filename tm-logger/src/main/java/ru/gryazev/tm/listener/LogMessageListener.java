package ru.gryazev.tm.listener;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.gryazev.tm.api.ILoggingService;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

@AllArgsConstructor
public class LogMessageListener implements MessageListener {

    @NotNull
    final ILoggingService loggingService;

    @Override
    @SneakyThrows
    public void onMessage(Message message) {
        if (message instanceof TextMessage) {
            loggingService.writeLog(((TextMessage) message));
        }
    }

}
