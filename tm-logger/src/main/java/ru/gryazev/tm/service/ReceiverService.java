package ru.gryazev.tm.service;

import lombok.Getter;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jms.*;

public class ReceiverService {

    @NotNull
    private final ConnectionFactory factory;

    @NotNull
    private final Connection connection;

    @NotNull
    private final Session session;

    @NotNull
    private final Destination destination;

    @Getter
    @NotNull
    private final MessageConsumer consumer;

    public ReceiverService() throws JMSException {
        this.factory = new ActiveMQConnectionFactory("tcp://localhost:61616");
        this.connection = factory.createConnection();
        this.connection.start();
        this.session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        this.destination = session.createTopic("CRUDLOGS");
        this.consumer = session.createConsumer(destination);
    }

}
