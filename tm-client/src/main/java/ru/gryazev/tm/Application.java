package ru.gryazev.tm;

import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.context.Bootstrap;

import java.util.Set;

public final class Application {

    public static void main(final String[] args) throws Exception {
        @NotNull final Set<Class<? extends AbstractCommand>> commandClasses =
                new Reflections("ru.gryazev.tm.command").getSubTypesOf(AbstractCommand.class);
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init(commandClasses);
    }

}
