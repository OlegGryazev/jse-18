package ru.gryazev.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    public String getProperty(@NotNull final String propertyKey) throws Exception;

}
