package ru.gryazev.tm.api.context;

import org.jetbrains.annotations.NotNull;
import ru.gryazev.tm.command.AbstractCommand;

import java.util.Map;

public interface CommandLocator {

    @NotNull
    public Map<String, AbstractCommand> getCommands();

}
