package ru.gryazev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.endpoint.ISystemEndpoint;

public class ServerInformationCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "server-info";
    }

    @Override
    public String getDescription() {
        return "Shows information about server host and port.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null || terminalService == null || sessionLocator == null) return;
        @Nullable final String token = getToken();
        @NotNull final ISystemEndpoint systemEndpoint = serviceLocator.getSystemEndpoint();
        @Nullable final String information = systemEndpoint.getServerInformation(token);
        terminalService.print(information);
    }

}
