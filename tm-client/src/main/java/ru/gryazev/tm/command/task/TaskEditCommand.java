package ru.gryazev.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.endpoint.ITaskEndpoint;
import ru.gryazev.tm.endpoint.IUserEndpoint;
import ru.gryazev.tm.endpoint.Task;
import ru.gryazev.tm.endpoint.User;
import ru.gryazev.tm.error.CrudInitializationException;
import ru.gryazev.tm.error.CrudNotFoundException;
import ru.gryazev.tm.error.CrudUpdateException;

@NoArgsConstructor
public final class TaskEditCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return "task-edit";
    }

    @Override
    public String getDescription() {
        return "Edit selected task.";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null || terminalService == null) return;
        @NotNull final ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        @NotNull final IUserEndpoint userEndpoint = serviceLocator.getUserEndpoint();
        @NotNull final String token = getToken();
        @NotNull final User currentUser = userEndpoint.findCurrentUser(token);
        if (currentUser == null) throw new CrudInitializationException();
        @Nullable String currentProjectId = getProjectId();
        final int taskIndex = terminalService.getTaskIndex();
        @Nullable final String taskId = taskEndpoint.getTaskId(token, currentProjectId, taskIndex);
        if (taskId == null) throw new CrudNotFoundException();

        @NotNull final Task task = terminalService.getTaskFromConsole();
        task.setUserId(currentUser.getId());
        task.setId(taskId);
        task.setProjectId(currentProjectId);
        @Nullable final Task editedTask = taskEndpoint.editTask(token, task);
        if (editedTask == null) throw new CrudUpdateException();
        terminalService.print("[OK]");
    }

}
