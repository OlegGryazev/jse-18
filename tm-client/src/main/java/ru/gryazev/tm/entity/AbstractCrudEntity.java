package ru.gryazev.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractCrudEntity implements Serializable {

    @NotNull
    private String id = UUID.randomUUID().toString();

    @Nullable
    public abstract String getUserId();

}
