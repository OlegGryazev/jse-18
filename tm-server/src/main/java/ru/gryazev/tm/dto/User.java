package ru.gryazev.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.entity.UserEntity;
import ru.gryazev.tm.enumerated.RoleType;

@Getter
@Setter
@NoArgsConstructor
public final class User extends AbstractCrudDTO {

    @Nullable
    private String login = "";

    @Nullable
    private String pwdHash = "";

    @Nullable
    private RoleType roleType;

    @Nullable
    private String name;

    @NotNull
    @Override
    @JsonIgnore
    public String getUserId() {
        return getId();
    }

    @Nullable
    public static UserEntity toUserEntity(
            @NotNull final ServiceLocator serviceLocator,
            @Nullable final User user
    ) {
        if (user == null) return null;
        @NotNull final UserEntity userEntity = new UserEntity();
        userEntity.setId(user.getId());
        userEntity.setLogin(user.getLogin());
        userEntity.setPwdHash(user.getPwdHash());
        userEntity.setName(user.getName());
        userEntity.setRoleType(user.getRoleType());
        userEntity.setProjects(serviceLocator.getProjectService().findByUserId(user.getId()));
        userEntity.setTasks(serviceLocator.getTaskService().findByUserId(user.getId()));
        userEntity.setSessions(serviceLocator.getSessionService().findByUserId(user.getId()));
        return userEntity;
    }

}
