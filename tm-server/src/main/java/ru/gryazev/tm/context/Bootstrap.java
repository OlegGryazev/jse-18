package ru.gryazev.tm.context;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import ru.gryazev.tm.api.context.ServiceLocator;
import ru.gryazev.tm.api.service.*;
import ru.gryazev.tm.constant.Constant;
import ru.gryazev.tm.endpoint.*;
import ru.gryazev.tm.service.*;

import javax.xml.ws.Endpoint;

public final class Bootstrap implements ServiceLocator {

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IEntityManagerService entityManagerService = new EntityManagerService();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(entityManagerService);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(entityManagerService);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(entityManagerService);

    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(entityManagerService);

    @Getter
    @NotNull
    private final ISystemService systemService = new SystemService();

    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint();

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint();

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint();

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint();

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint();

    public void init() {
        projectEndpoint.setServiceLocator(this);
        taskEndpoint.setServiceLocator(this);
        userEndpoint.setServiceLocator(this);
        sessionEndpoint.setServiceLocator(this);
        systemEndpoint.setServiceLocator(this);
        @NotNull final String addressPattern = String.format("http://%s:%s/%s", Constant.HOST, Constant.PORT, "%s?wsdl");
        Endpoint.publish(String.format(addressPattern, "session"), sessionEndpoint);
        Endpoint.publish(String.format(addressPattern, "project"), projectEndpoint);
        Endpoint.publish(String.format(addressPattern, "task"), taskEndpoint);
        Endpoint.publish(String.format(addressPattern, "user"), userEndpoint);
        Endpoint.publish(String.format(addressPattern, "system"), systemEndpoint);
    }

}