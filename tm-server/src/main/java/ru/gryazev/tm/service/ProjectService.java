package ru.gryazev.tm.service;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.repository.IProjectRepository;
import ru.gryazev.tm.api.service.IEntityManagerService;
import ru.gryazev.tm.api.service.IProjectService;
import ru.gryazev.tm.entity.ProjectEntity;
import ru.gryazev.tm.repository.ProjectRepository;
import ru.gryazev.tm.util.CompareUtil;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

@AllArgsConstructor
public final class ProjectService implements IProjectService {

    @NotNull
    final IEntityManagerService entityManagerService;

    @Nullable
    public String getProjectId(@Nullable final String userId, final int projectIndex) {
        if (userId == null || userId.isEmpty() || projectIndex < 0) return null;
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @Nullable final List<ProjectEntity> projectEntities = projectRepository.findAllByUserId(userId);
        entityManager.close();
        return projectIndex >= projectEntities.size() ? null : projectEntities.get(projectIndex).getId();
    }

    @Nullable
    @Override
    public ProjectEntity create(@Nullable final String userId, @Nullable final ProjectEntity projectEntity) {
        if (userId == null || userId.isEmpty()) return null;
        if (!isEntityValid(projectEntity)) return null;
        if (!userId.equals(projectEntity.getUser().getId())) return null;
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        entityManager.getTransaction().begin();
        projectRepository.persist(projectEntity);
        entityManager.getTransaction().commit();
        entityManager.close();
        return projectEntity;
    }

    @Nullable
    @Override
    public ProjectEntity edit(@Nullable final String userId, @Nullable final ProjectEntity projectEntity) throws Exception {
        if (!isEntityValid(projectEntity)) return null;
        if (userId == null || userId.isEmpty()) return null;
        if (!userId.equals(projectEntity.getUser().getId())) return null;
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        entityManager.getTransaction().begin();
        projectRepository.merge(projectEntity);
        entityManager.getTransaction().commit();
        entityManager.close();
        return projectEntity;
    }

    @Nullable
    @Override
    public ProjectEntity findOne(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) return null;
        if (projectId == null || projectId.isEmpty()) return null;
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @Nullable final ProjectEntity projectEntity = projectRepository.findOneById(userId, projectId);
        entityManager.close();
        return projectEntity;
    }

    @NotNull
    @Override
    public List<ProjectEntity> findByName(@Nullable final String userId, @Nullable final String projectName) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (projectName == null || projectName.isEmpty()) return Collections.emptyList();
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @NotNull final List<ProjectEntity> projects = projectRepository.findAllByName(userId, projectName);
        entityManager.close();
        return projects;
    }

    @NotNull
    @Override
    public List<ProjectEntity> findByDetails(@Nullable final String userId, @Nullable final String projectDetails) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (projectDetails == null || projectDetails.isEmpty()) return Collections.emptyList();
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @NotNull final List<ProjectEntity> projects = projectRepository.findAllByDetails(userId, projectDetails);
        entityManager.close();
        return projects;
    }

    @NotNull
    @Override
    public List<ProjectEntity> findByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @NotNull final List<ProjectEntity> list = projectRepository.findAllByUserId(userId);
        entityManager.close();
        return list;
    }

    @NotNull
    @Override
    public List<ProjectEntity> findByUserIdSorted(@Nullable final String userId, @Nullable final String sortType) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final String sqlSortType = CompareUtil.getSqlSortType(sortType);
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @NotNull final List<ProjectEntity> list = projectRepository.findAllByUserIdSorted(userId, sqlSortType);
        entityManager.close();
        return list;
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) return;
        if (projectId == null || projectId.isEmpty()) return;
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        entityManager.getTransaction().begin();
        projectRepository.removeById(userId, projectId);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        entityManager.getTransaction().begin();
        projectRepository.removeAllByUserId(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAll() {
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        entityManager.getTransaction().begin();
        projectRepository.removeAll();
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @NotNull
    @Override
    public List<ProjectEntity> findAll() {
        @NotNull final EntityManager entityManager = entityManagerService.getEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @NotNull final List<ProjectEntity> projects = projectRepository.findAll();
        entityManager.close();
        return projects;
    }

    private boolean isEntityValid(@Nullable final ProjectEntity projectEntity) {
        if (projectEntity == null) return false;
        if (projectEntity.getId() == null || projectEntity.getId().isEmpty()) return false;
        if (projectEntity.getName() == null || projectEntity.getName().isEmpty()) return false;
        return projectEntity.getUser().getId() != null && !projectEntity.getUser().getId().isEmpty();
    }

}
