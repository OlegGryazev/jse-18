package ru.gryazev.tm.service;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import ru.gryazev.tm.api.service.IMessageProducerService;

import javax.jms.*;

public class MessageProducerService implements IMessageProducerService {

    @NotNull
    private final ActiveMQConnectionFactory connectionFactory;

    @NotNull
    private final Connection connection;

    public MessageProducerService() throws JMSException {
        this.connectionFactory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_BROKER_URL);
        this.connection = connectionFactory.createConnection();
        connection.start();
    }

    public void sendMessage(@NotNull final String message) throws JMSException {
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Destination destination = session.createTopic("CRUDLOGS");
        @NotNull final MessageProducer producer = session.createProducer(destination);

        @NotNull final TextMessage textMessage = session.createTextMessage();
        textMessage.setText(message);
        producer.send(textMessage);
    }

}
