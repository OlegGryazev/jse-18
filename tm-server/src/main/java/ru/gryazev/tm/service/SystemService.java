package ru.gryazev.tm.service;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.service.ISystemService;
import ru.gryazev.tm.constant.Constant;

@AllArgsConstructor
public class SystemService implements ISystemService {

    @Nullable
    public String getServerInformation() {
        return String.format("%s : %s", Constant.HOST, Constant.PORT);
    }

}
