package ru.gryazev.tm.service;

import org.apache.activemq.broker.BrokerService;
import org.jetbrains.annotations.NotNull;
import ru.gryazev.tm.api.context.ServiceLocator;

public class JmsBrokerService {

    @NotNull
    final ServiceLocator serviceLocator;

    @NotNull
    final BrokerService broker = new BrokerService();

    public JmsBrokerService(@NotNull final ServiceLocator serviceLocator) throws Exception {
        this.serviceLocator = serviceLocator;
        broker.addConnector(serviceLocator.getPropertyService().getProperty("broker-url"));
        broker.start();
    }

}
