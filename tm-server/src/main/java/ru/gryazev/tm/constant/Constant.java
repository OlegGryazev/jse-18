package ru.gryazev.tm.constant;

import org.jetbrains.annotations.NotNull;

import java.util.UUID;

public class Constant {

    @NotNull
    public final static String SALT = "7f35749e-5a6a-4224-b954-3db7c18a6d79";

    public final static int CYCLE_COUNT = 10;

    public final static long SESSION_LIFETIME = 3600000;

    public final static String DATA_DIR = System.getProperty("user.dir") + "/tm-server/data/";

    public final static String HOST = "cluster";

    public final static String PORT = System.getProperty("server.port") == null ? "8080" : System.getProperty("server.port");

}
