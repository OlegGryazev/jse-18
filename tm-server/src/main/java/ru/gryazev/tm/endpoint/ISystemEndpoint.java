package ru.gryazev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.api.context.ServiceLocator;

import javax.jws.WebService;

@WebService
public interface ISystemEndpoint {

    @Nullable
    public String getServerInformation(@Nullable String token) throws Exception;

    public void setServiceLocator(@NotNull ServiceLocator serviceLocator);

}
