package ru.gryazev.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class CompareUtil {

    @NotNull
    public static String getSqlSortType(@Nullable final String sortType) {
        if (sortType == null) return "timestamp";
        switch (sortType) {
            case "default": return "timestamp";
            case "date-start": return "date_start";
            case "date-finish": return "date_finish";
            case "status": return "status";
        }
        return "timestamp";
    }

}
